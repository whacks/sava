package org.rajivprab.sava.rest;

import org.junit.Test;
import org.rajivprab.sava.TestBase;
import org.rajivprab.sava.logging.Severity;

import static com.google.common.truth.Truth.assertThat;

public class LoggableWebAppExceptionTest extends TestBase {
    @Test
    public void reportLoggableWebAppException_shouldPrintRelevantFields() {
        String errorMessage = "test message";
        LOG_DISPATCHER.report(new IllegalUserInputException(errorMessage));

        assertThat(DISPATCHER.getDispatches()).hasSize(1);
        String body = DISPATCHER.getDispatches(Severity.WARN).get(errorMessage);
        assertThat(body).contains("IllegalUserInputException");
        assertThat(body).contains(errorMessage);
        assertThat(body).contains("status=400");
        assertThat(body).contains("LoggableWebAppExceptionTest.java:13");
    }
}
