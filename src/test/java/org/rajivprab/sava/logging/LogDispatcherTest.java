package org.rajivprab.sava.logging;

import com.google.common.truth.Truth;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.junit.Test;
import org.rajivprab.cava.Randomc;
import org.rajivprab.sava.TestBase;

public class LogDispatcherTest extends TestBase {
    private static final Exception EXCEPTION = new ArithmeticException("exception-msg");

    @Test
    public void logBothMessageAndThrowable() {
        LOG_DISPATCHER.report(this, Severity.WARN, "msg", EXCEPTION);
        Truth.assertThat(DISPATCHER.getDispatches(Severity.WARN))
             .containsExactly("msg", "msg\n" + ExceptionUtils.getStackTrace(EXCEPTION));
    }

    @Test
    public void shouldRedirectCallsWithNullThrowable() {
        LOG_DISPATCHER.report(this, Severity.WARN, "msg", null);
        Truth.assertThat(DISPATCHER.getDispatches(Severity.WARN)).containsExactly("msg", "msg");
    }

    @Test
    public void shouldRedirectCallsWithNullMessage() {
        LOG_DISPATCHER.report(this, Severity.WARN, null, EXCEPTION);
        Truth.assertThat(DISPATCHER.getDispatches(Severity.WARN))
             .containsExactly(EXCEPTION.toString(), ExceptionUtils.getStackTrace(EXCEPTION));
    }

    @Test
    public void shouldRedirectCallsWithEmptyMessage() {
        LOG_DISPATCHER.report(this, Severity.WARN, "", EXCEPTION);
        Truth.assertThat(DISPATCHER.getDispatches(Severity.WARN))
             .containsExactly(EXCEPTION.toString(), ExceptionUtils.getStackTrace(EXCEPTION));
    }

    @Test
    public void logAllSeverities() {
        for (var severity : Severity.values()) {
            LOG_DISPATCHER.report(this, severity, "", EXCEPTION);
            Truth.assertThat(DISPATCHER.getDispatches(severity))
                 .containsExactly(EXCEPTION.toString(), ExceptionUtils.getStackTrace(EXCEPTION));

            DISPATCHER.clear();

            var message = Randomc.phrase();
            LOG_DISPATCHER.report(this, severity, message, EXCEPTION);
            Truth.assertThat(DISPATCHER.getDispatches(severity))
                 .containsExactly(message, message + "\n" + ExceptionUtils.getStackTrace(EXCEPTION));

            DISPATCHER.clear();

            message = Randomc.phrase();
            LOG_DISPATCHER.report(this, severity, message);
            Truth.assertThat(DISPATCHER.getDispatches(severity)).containsExactly(message, message);
        }
    }
}
