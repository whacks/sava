package org.rajivprab.sava.logging;

import com.google.common.truth.Truth;
import org.junit.Test;
import org.rajivprab.sava.TestBase;

/**
 * Created by rajivprab on 7/3/17.
 */
public class DispatcherTest extends TestBase {
    @Test
    public void bufferedDispatcher_getAndClear() {
        BufferedDispatcher dispatcher = Dispatcher.getBufferedDispatcher().ignore(Severity.INFO);

        dispatcher.dispatch(Severity.ERROR, "err");
        dispatcher.dispatch(Severity.WARN, "warning 1");
        dispatcher.dispatch(Severity.WARN, "warning 2");
        dispatcher.dispatch(Severity.INFO, "should ignore");

        Truth.assertThat(dispatcher.getDispatches(Severity.ERROR)).containsExactly("err", "err");
        Truth.assertThat(dispatcher.getDispatches(Severity.WARN))
             .containsExactly("warning 1", "warning 1", "warning 2", "warning 2");
        Truth.assertThat(dispatcher.getDispatches(Severity.INFO)).isEmpty();

        Truth.assertThat(dispatcher.getDispatches()).hasSize(3);
        Truth.assertThat(dispatcher.getDispatches())
             .containsCell(Severity.ERROR, "err", "err");
        Truth.assertThat(dispatcher.getDispatches())
             .containsCell(Severity.WARN, "warning 1", "warning 1");
        Truth.assertThat(dispatcher.getDispatches())
             .containsCell(Severity.WARN, "warning 2", "warning 2");

        dispatcher.clear();
        Truth.assertThat(dispatcher.getDispatches()).isEmpty();
    }
}
