package org.rajivprab.sava.s3;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.google.common.truth.Truth;
import org.apache.jasper.tagplugins.jstl.core.Url;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.rajivprab.cava.FileUtilc;
import org.rajivprab.cava.IOUtilc;
import org.rajivprab.sava.TestBase;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.Duration;
import java.util.UUID;

/**
 * Tests will only work if you have the following AWS keys set up in your environment. Ie, it will only work for Rajiv.
 * TODO: Create a free AWS account and use that to write a publicly runnable integration test
 * <p>
 * Created by rajivprab on 6/4/17.
 */
public class S3InterfaceIT extends TestBase {
    private static final String BUCKET = "rajivprab-credentials-qa";
    private static final String FILE_PATH = "encryption/encryption_keys.json";
    private static final Regions REGION = Regions.US_EAST_1;

    private static final AWSCredentialsProvider CREDENTIALS_PROVIDER = AwsCredentials.customEnvironmentVariable(
            "PERSONAL_S3READONLY_ACCESS_KEY_ID",
            "PERSONAL_S3READONLY_SECRET_ACCESS_KEY");

    private static final S3Interface S3_INTERFACE_DEPRECATED = S3Interface.get(CREDENTIALS_PROVIDER);
    private static final S3Interface S3_INTERFACE_REGION = S3Interface.get(CREDENTIALS_PROVIDER, REGION);

    @Test
    public void readSomeFile_deprecatedConstructor() {
        Truth.assertThat(S3_INTERFACE_DEPRECATED.getFullContents(BUCKET, FILE_PATH))
             .contains("KEY_NUM_BITS");
    }

    @Test
    public void readSomeFile_withRegion() {
        Truth.assertThat(S3_INTERFACE_REGION.getFullContents(BUCKET, FILE_PATH)).contains("KEY_NUM_BITS");
    }

    @Test
    public void getPresignedUrl_tryDownloadingIt_shouldWork() throws Exception {
        URL url = S3_INTERFACE_REGION.getPresignedUrl(BUCKET, FILE_PATH, Duration.ofMinutes(1));
        BufferedInputStream in = new BufferedInputStream(url.openStream());
        JSONObject downloaded = new JSONObject(IOUtilc.toString(in));
        Truth.assertThat(downloaded.getInt("KEY_NUM_BITS")).isEqualTo(256);
    }

    @Test
    public void getUnsignedUrl_tryDownloadingIt_shouldFail() throws Exception {
        String unsignedUrl = S3_INTERFACE_REGION.getURL(BUCKET, FILE_PATH);
        try {
            new URL(unsignedUrl).openStream();
            Assert.fail("Should have thrown error");
        } catch (IOException e) {
            Truth.assertThat(e.getMessage()).contains("Server returned HTTP response code: 403");
        }
    }

    @Test
    public void getUrl() {
        String url = S3_INTERFACE_REGION.getURL(BUCKET, FILE_PATH);
        Truth.assertThat(url).isEqualTo(String.format("https://%s.s3.amazonaws.com/%s", BUCKET, FILE_PATH));
    }

    @Test
    public void getUrl_withSpaces() {
        Truth.assertThat(S3_INTERFACE_REGION.getURL("this is my bucket", "file / with spaces"))
             .isEqualTo("https://s3.amazonaws.com/this%20is%20my%20bucket/file%20/%20with%20spaces");
    }

    @Test(expected = AmazonS3Exception.class)
    public void fileNotFound_throwsException() {
        S3_INTERFACE_REGION.getS3Stream(BUCKET, "no/such/file");
    }

    @Test
    public void downloadFile() {
        File outputFile = new File("/tmp/" + UUID.randomUUID());
        outputFile.deleteOnExit();
        S3_INTERFACE_REGION.downloadFile(BUCKET, FILE_PATH, outputFile);
        Truth.assertThat(FileUtilc.readFileToString(outputFile)).contains("KEY_NUM_BITS");
    }
}
