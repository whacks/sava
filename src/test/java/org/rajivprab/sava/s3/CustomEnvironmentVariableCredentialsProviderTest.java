package org.rajivprab.sava.s3;

import com.amazonaws.auth.AWSCredentialsProvider;
import org.junit.Assert;
import org.junit.Test;
import org.rajivprab.cava.Mapc;
import org.rajivprab.sava.TestBase;

import java.lang.reflect.Field;
import java.util.Collections;
import java.util.Map;

import static com.google.common.truth.Truth.assertThat;

public class CustomEnvironmentVariableCredentialsProviderTest extends TestBase {
    @Test
    public void environmentVariableSet_allGood() throws Exception {
        setEnvironmentVariables(Mapc.newHashMap("id", "my-id", "secret", "my secret"));
        AWSCredentialsProvider provider = AwsCredentials.customEnvironmentVariable(
                "id", "secret");

        assertThat(provider.getCredentials().getAWSAccessKeyId()).isEqualTo("my-id");
        assertThat(provider.getCredentials().getAWSSecretKey()).isEqualTo("my secret");
    }

    @Test
    public void environmentVariableNotSet_shouldThrowErrorDuringGet() {
        AWSCredentialsProvider provider = AwsCredentials.customEnvironmentVariable(
                "does not", "exist");
        provider.refresh();
        try {
            provider.getCredentials();
            Assert.fail();
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageThat().isEqualTo("No environment variable found for: does not");
        }
    }

    // https://stackoverflow.com/a/7201825/4816322
    private static void setEnvironmentVariables(Map<String, String> environmentVariables) throws Exception {
        try {
            Class<?> processEnvironmentClass = Class.forName("java.lang.ProcessEnvironment");
            Field theEnvironmentField = processEnvironmentClass.getDeclaredField("theEnvironment");
            theEnvironmentField.setAccessible(true);
            Map<String, String> env = (Map<String, String>) theEnvironmentField.get(null);
            env.putAll(environmentVariables);
            Field theCaseInsensitiveEnvironmentField =
                    processEnvironmentClass.getDeclaredField("theCaseInsensitiveEnvironment");
            theCaseInsensitiveEnvironmentField.setAccessible(true);
            Map<String, String> cienv = (Map<String, String>)     theCaseInsensitiveEnvironmentField.get(null);
            cienv.putAll(environmentVariables);
        } catch (NoSuchFieldException e) {
            Class[] classes = Collections.class.getDeclaredClasses();
            Map<String, String> env = System.getenv();
            for(Class cl : classes) {
                if("java.util.Collections$UnmodifiableMap".equals(cl.getName())) {
                    Field field = cl.getDeclaredField("m");
                    field.setAccessible(true);
                    Object obj = field.get(env);
                    Map<String, String> map = (Map<String, String>) obj;
                    map.clear();
                    map.putAll(environmentVariables);
                }
            }
        }
    }
}
