package org.rajivprab.sava.events;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.model.AmazonSNSException;
import com.google.common.truth.Truth;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.rajivprab.sava.TestBase;

/**
 * Integration tests for SNSPublisher
 * TODO Set up new SNS topics just for sava testing
 *
 * Created by rajivprab on 6/4/17.
 */
public class PublisherIT extends TestBase {
    private static final String TOPIC_DUMMY = "arn:aws:sns:us-east-1:444813972869:dummy";
    private static final JSONObject CREDENTIALS = new JSONObject(apiKeys().getCredentials("sns/sns_credentials.json"));
    private static final Publisher PUBLISHER =
            Publisher.getSNS(CREDENTIALS.getString("Access_Key_Id"),
                             CREDENTIALS.getString("Secret_Access_Key"));

    @Test
    public void publish() {
        PUBLISHER.publish(TOPIC_DUMMY, "test title", "test message");
    }

    @Test
    public void publishWithRegion() {
        Publisher.getSNS(CREDENTIALS.getString("Access_Key_Id"),
                         CREDENTIALS.getString("Secret_Access_Key"),
                         Regions.US_EAST_1)
                 .publish(TOPIC_DUMMY, "test title", "test message");
    }

    @Test
    public void badTopicGiven_willThrowOneOfSeveralDifferentAmazonExceptions() {
        try {
            PUBLISHER.publish("arn:aws:sns:us-east-1:37287331:random_topic", "abc", "xyz");
            Assert.fail("Should have failed");
        } catch (AmazonSNSException e) {
            Truth.assertThat(e).hasMessageThat().contains("No account found for the given parameters");
        }
    }
}
