package org.rajivprab.sava.login;

import com.amazonaws.util.StringInputStream;
import com.google.common.truth.Truth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.UserRecord;
import javax.ws.rs.core.Response.Status;
import org.apache.commons.lang3.RandomStringUtils;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.rajivprab.cava.Randomc;
import org.rajivprab.sava.TestBase;
import org.rajivprab.sava.login.AuthFirebase.AuthFirebaseException;

import java.util.concurrent.ThreadLocalRandom;

import static com.google.common.truth.Truth.assertThat;

public class AuthFirebaseIT extends TestBase {
    // Get from https://console.firebase.google.com/u/0/project/sava-testing/authentication/users
    // If accidentally deleted, create a new one for testing purposes, by commenting out the "deleteUser" call below
    private static final String EXISTING_USER_ID = "Cf31Wsr60i";
    private static final String EXISTING_EMAIL = "whackri+sava_testing_firebase_cf31wsr60i@gmail.com";

    @BeforeClass
    public static void setup() throws Exception {
        String credentials = apiKeys().getCredentials("firebase/sava-testing-firebase-adminsdk-jdri6.json");
        AuthFirebase.init("sava-testing", new StringInputStream(credentials));
    }

    // ------------------------------------------------

    @Test
    public void getExistingUserById() {
        assertThat(AuthFirebase.getUserByID(EXISTING_USER_ID).getEmail()).isEqualTo(EXISTING_EMAIL);
    }

    @Test
    public void getExistingUserByEmail() {
        assertThat(AuthFirebase.getUserByEmail(EXISTING_EMAIL).getUid()).isEqualTo(EXISTING_USER_ID);
    }

    @Test
    public void invalidUserId_shouldThrowFirebaseAuthException() {
        checkUserNotFound(EXISTING_USER_ID.toLowerCase());
    }

    // --------------- Create/Delete Users ------------

    @Test
    public void create_get_update_andDelete_newUser() throws Exception {
        String password = RandomStringUtils.randomAlphanumeric(10);
        String userID = RandomStringUtils.randomAlphanumeric(10);
        String email = "whackri+sava_testing_firebase_" + userID + "@gmail.com";
        UserRecord createUserRecord = AuthFirebase.createUser(userID, email, password);

        assertThat(createUserRecord.getEmail()).isEqualTo(email.toLowerCase());
        assertThat(createUserRecord.getUid()).isEqualTo(userID);
        assertThat(createUserRecord.isDisabled()).isFalse();
        assertThat(createUserRecord.isEmailVerified()).isFalse();

        // TODO Enhancement - Unable to create an ID-token, for use in the verify methods
        // Following guide doesn't work. There's no getCurrentUser method available:
        // https://firebase.google.com/docs/auth/android/start/
        // Using createCustomToken(userID) produces a token that's incompatible with verifyIdToken

        verifyEqual(AuthFirebase.getUserByID(userID), createUserRecord);
        verifyEqual(AuthFirebase.getUserByEmail(email), createUserRecord);

        String newEmail = "whackri+sava_testing_firebase_" + RandomStringUtils.randomAlphanumeric(10) + "@gmail.com";
        AuthFirebase.updateEmail(userID, newEmail);
        checkEmailNotFound(email);
        UserRecord record = AuthFirebase.getUserByID(userID);
        assertThat(record.getEmail()).isEqualTo(newEmail.toLowerCase());
        assertThat(record.getUid()).isEqualTo(userID);

        AuthFirebase.deleteUser(userID);
        checkUserNotFound(userID);
    }

    @Test
    public void deleteExistingUser_andCreateAgain() {
        AuthFirebase.deleteUser(EXISTING_USER_ID);

        String password = RandomStringUtils.randomAlphanumeric(10);
        AuthFirebase.createUser(EXISTING_USER_ID, EXISTING_EMAIL, password);

        getExistingUserByEmail();
        getExistingUserById();
    }

    @Test
    public void create_reuseExistingUserName_shouldFail() {
        try {
            AuthFirebase.createUser(EXISTING_USER_ID, "thecaucusnet@gmail.com", "password");
            Assert.fail("Should have failed");
        } catch (AuthFirebaseException e) {
            checkAuthFirebaseException(e, FirebaseAuthException.class, "ALREADY_EXISTS: DUPLICATE_LOCAL_ID");
        }
    }

    @Test
    public void create_reuseExistingEmail_shouldFail() {
        try {
            AuthFirebase.createUser("thecaucusnet", EXISTING_EMAIL, "password");
            Assert.fail("Should have failed");
        } catch (AuthFirebaseException e) {
            checkAuthFirebaseException(e, FirebaseAuthException.class, "ALREADY_EXISTS: EMAIL_EXISTS");
        }
    }

    // ----------------- Set password -----------------

    @Test
    public void setPassword_shouldWork() {
        AuthFirebase.setPassword(EXISTING_USER_ID, Randomc.phrase(6));
    }

    @Test
    public void setPassword_verySimple_stillWorks() {
        AuthFirebase.setPassword(EXISTING_USER_ID, "1234567");
    }

    @Test
    public void setPassword_tooShort() {
        try {
            AuthFirebase.setPassword(EXISTING_USER_ID, Randomc.phrase(5).substring(0, 5));
            Assert.fail("Should have thrown exception");
        } catch (AuthFirebaseException e) {
            Truth.assertThat(e).hasMessageThat().isEqualTo("password must be at least 6 characters long");
        }
    }

    // ----------------- Update email -----------------

    @Test
    public void updateEmail_invalidEmail_shouldThrowIllegalArgumentException_noMessage() {
        try {
            AuthFirebase.updateEmail(EXISTING_USER_ID, RandomStringUtils.randomAlphanumeric(5));
            Assert.fail("Should have thrown error");
        } catch (IllegalArgumentException e) {
            // This exception is thrown by Firebase, not the wrapper. No good way to catch and check for it in wrapper
            Truth.assertThat(e).hasMessageThat().isNull();
        }
    }

    @Test
    public void updateEmail_invalidUserID_shouldThrowAuthFirebaseException() {
        try {
            AuthFirebase.updateEmail(RandomStringUtils.randomAlphanumeric(5), EXISTING_EMAIL);
            Assert.fail("Should have thrown error");
        } catch (AuthFirebaseException e) {
            Truth.assertThat(e).hasMessageThat().startsWith("No user record found for the provided user ID");
        }
    }

    @Test
    public void updateEmail_exactSameEmail_shouldStillWork() {
        AuthFirebase.updateEmail(EXISTING_USER_ID, EXISTING_EMAIL);
        assertThat(AuthFirebase.getUserByID(EXISTING_USER_ID).getEmail()).isEqualTo(EXISTING_EMAIL);
        assertThat(AuthFirebase.getUserByEmail(EXISTING_EMAIL).getUid()).isEqualTo(EXISTING_USER_ID);
    }

    // ----------------- Verify -----------------

    @Test
    public void verifyNullOrEmptyToken() {
        String badToken = ThreadLocalRandom.current().nextBoolean() ? null : "";
        try {
            AuthFirebase.verifyIdToken(badToken);
            Assert.fail("Should have thrown exception");
        } catch (AuthFirebaseException e) {
            checkAuthFirebaseException(e, IllegalArgumentException.class, "Bad firebase-id-token: " + badToken);
        }
    }

    @Test
    public void verify_slightlyMalformedToken_shouldThrowExecException_withCauseIllegalArgumentException() {
        // In previous versions of the library, we saw different outputs for different malformed strings
        String token = ThreadLocalRandom.current().nextBoolean() ?
                "123.abc.def" : "123";
        try {
            AuthFirebase.verifyIdToken(token);
            Assert.fail("Should have failed");
        } catch (AuthFirebaseException e) {
            checkAuthFirebaseException(
                    e, FirebaseAuthException.class,
                    "Failed to parse Firebase ID token. Make sure you passed a string that represents a complete and valid JWT. See https://firebase.google.com/docs/auth/admin/verify-id-tokens for details on how to retrieve an ID token.");
        }
    }

    // ------------------- helpers ----------

    private static void checkUserNotFound(String userID) {
        try {
            AuthFirebase.getUserByID(userID);
            Assert.fail("Exception should have been thrown");
        } catch (AuthFirebaseException e) {
            checkAuthFirebaseException(e, FirebaseAuthException.class,
                                       "No user record found for the provided user ID: " + userID);
        }
    }

    private static void checkEmailNotFound(String email) {
        try {
            AuthFirebase.getUserByEmail(email);
            Assert.fail("Exception should have been thrown");
        } catch (AuthFirebaseException e) {
            checkAuthFirebaseException(e, FirebaseAuthException.class,
                    "No user record found for the provided email: " + email);
        }
    }

    private static void checkAuthFirebaseException(AuthFirebaseException e, Class cause, String message) {
        assertThat(e.getCause()).isInstanceOf(cause);
        assertThat(e.getMessage()).isEqualTo(message);
        assertThat(e.getResponse().getStatusInfo()).isEqualTo(Status.UNAUTHORIZED);
        assertThat(e.getResponse().getEntity()).isEqualTo(new JSONObject().put("message", message).toString());
    }

    private static void verifyEqual(UserRecord actual, UserRecord expected) {
        assertThat(actual.getEmail()).isEqualTo(expected.getEmail());
        assertThat(actual.getUid()).isEqualTo(expected.getUid());
        assertThat(actual.isDisabled()).isEqualTo(expected.isDisabled());
        assertThat(actual.isEmailVerified()).isEqualTo(expected.isEmailVerified());
    }
}
