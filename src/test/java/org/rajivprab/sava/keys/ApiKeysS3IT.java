package org.rajivprab.sava.keys;

import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.google.common.truth.Truth;
import org.junit.Test;
import org.rajivprab.sava.TestBase;

/**
 * Integration tests for the ApiKeys module
 *
 * Created by rajivprab on 6/4/17.
 */
public class ApiKeysS3IT extends TestBase {
    @Test
    public void downloadKeys() {
        Truth.assertThat(apiKeys().getCredentials("encryption/encryption_keys.json")).contains("KEY_NUM_BITS");
    }

    @Test (expected = AmazonS3Exception.class)
    public void keysNotFound_throwsException() {
        apiKeys().getCredentials("file/does/not/exist");
    }
}
