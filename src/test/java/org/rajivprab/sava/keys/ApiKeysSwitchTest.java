package org.rajivprab.sava.keys;

import com.google.common.collect.ImmutableTable;
import com.google.common.collect.Table;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.rajivprab.sava.TestBase;
import org.rajivprab.sava.s3.FakeS3Interface;
import org.rajivprab.sava.s3.S3Interface;
import org.rajivprab.sava.threading.ThreadHelper;

import java.util.concurrent.Executors;

import static com.google.common.truth.Truth.assertThat;

/**
 * Unit tests for ApiKeysMux
 *
 * Created by rajivprab on 5/19/17.
 */
public class ApiKeysSwitchTest extends TestBase {
    private static final String PROD_BUCKET = "prod";
    private static final String QA_BUCKET = "qa";

    private static final String KEY_1 = "abc";
    private static final String KEY_2 = "ein";
    private static final String KEY_3 = "blue";
    private static final String VALUE_1 = "def";
    private static final String VALUE_2 = "zwei";
    private static final String VALUE_3 = "red";

    private static final Table<String, String, String> FAKE_S3 =
            ImmutableTable.<String, String, String>builder()
                    .put(QA_BUCKET, KEY_1, VALUE_1)
                    .put(QA_BUCKET, KEY_2, VALUE_2)
                    .put(QA_BUCKET, KEY_3, VALUE_3)
                    .put(PROD_BUCKET, KEY_1, VALUE_1.toUpperCase())
                    .put(PROD_BUCKET, KEY_2, VALUE_2.toUpperCase())
                    .put(PROD_BUCKET, KEY_3, VALUE_3.toUpperCase())
                    .build();

    private static final ThreadHelper HELPER =
            ThreadHelper.build(Executors.newFixedThreadPool(10), true);

    @After
    public void after() {
        HELPER.checkTasks();
    }

    @Test
    public void useQaFollowedByMultipleParallelReads() {
        ApiKeysSwitch mux = getApiKeysMux();
        mux.useQA();
        HELPER.execute(() -> assertThat(mux.getCredentials(KEY_1)).isEqualTo(VALUE_1));
        HELPER.execute(() -> assertThat(mux.getCredentials(KEY_2)).isEqualTo(VALUE_2));
        HELPER.execute(() -> assertThat(mux.getCredentials(KEY_3)).isEqualTo(VALUE_3));
    }

    @Test
    public void readFollowedByUseQa_shouldThrowError() {
        ApiKeysSwitch mux = getApiKeysMux();
        assertThat(mux.getCredentials(KEY_1)).isEqualTo(VALUE_1.toUpperCase());
        try {
            mux.useQA();
            Assert.fail("Should have hit an error");
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageThat().contains("Prod credentials have already been used prior to useQA");
        }
    }

    @Test
    public void multipleUseQaCalls() {
        useQaFollowedByMultipleParallelReads();
        useQaFollowedByMultipleParallelReads();
    }

    private static ApiKeysSwitch getApiKeysMux() {
        S3Interface s3 = new FakeS3Interface(FAKE_S3);
        return ApiKeysSwitch.build(ApiKeysS3.build(PROD_BUCKET, s3), ApiKeysS3.build(QA_BUCKET, s3));
    }
}
