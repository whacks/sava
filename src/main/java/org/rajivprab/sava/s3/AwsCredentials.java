package org.rajivprab.sava.s3;

import com.amazonaws.auth.AWSCredentialsProvider;

public class AwsCredentials {
    // Throws IllegalArgumentException if the environment-variable is not set, when getCredentials is called
    public static AWSCredentialsProvider customEnvironmentVariable(String accessKeyIDVar, String secretAccessKeyVar) {
        return new CustomEnvironmentVariableCredentialsProvider(accessKeyIDVar, secretAccessKeyVar);
    }
}
