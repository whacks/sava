package org.rajivprab.sava.s3;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.util.StringUtils;
import org.rajivprab.cava.Validatec;

/**
 * Allows providing the aws credentials through a user-specified env-variable. Thread-safe
 *
 * Mostly copied from com.amazonaws.auth.EnvironmentVariableCredentialsProvider,
 * except that the user can specify which env-var to use
 *
 * Arguably this can be replaced with:
 * new AWSStaticCredentialsProvider(new BasicAWSCredentials(System.getenv(accessKeyIDVar),
 *                                                          System.getenv(secretAccessKeyVar)))
 * The above would work if we don't need the ability to dynamically change the env-var value
 *
 * Created by rprabhakar on 2/2/16.
 */
class CustomEnvironmentVariableCredentialsProvider implements AWSCredentialsProvider {
    private final String accessKeyIDVar;
    private final String secretAccessKeyVar;

    CustomEnvironmentVariableCredentialsProvider(String accessKeyIDVar, String secretAccessKeyVar) {
        this.accessKeyIDVar = accessKeyIDVar;
        this.secretAccessKeyVar = secretAccessKeyVar;
    }

    // Throws IllegalArgumentException if the environment-variable is not set
    @Override
    public AWSCredentials getCredentials() {
        return new BasicAWSCredentials(getEnv(accessKeyIDVar), getEnv(secretAccessKeyVar));
    }

    @Override
    public void refresh() {}

    private static String getEnv(String var) {
        return Validatec.notEmpty(StringUtils.trim(System.getenv(var)), "No environment variable found for: " + var);
    }
}
