package org.rajivprab.sava.database;

import org.jooq.DSLContext;
import org.jooq.impl.DSL;
import org.rajivprab.cava.FileUtilc;

import java.sql.Connection;

/**
 * Interface for connecting to the database
 * <p>
 * Created by rprabhakar on 6/20/15.
 */
public class Database {
    private final ConcurrentConnectionPool pool;

    public Database(ConcurrentConnectionPool concurrentConnectionPool) {
        this.pool = concurrentConnectionPool;
    }

    // Only to be used with trusted files in ClassPath. Do not use for client-provided inputs
    public int executeSqlFile(String path) {
        return getDSLContext().execute(FileUtilc.readClasspathFile(path));
    }

    // TODO Enhancement - Cache the DSLContext in a pool, instead of regenerating everytime?
    public DSLContext getDSLContext() {
        return DSL.using(getConnection());
    }

    public void closeAllConnections() {
        pool.closeAllConnections();
    }

    public Connection getConnection() {
        return pool.leech();
    }
}
