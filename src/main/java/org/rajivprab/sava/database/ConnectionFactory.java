package org.rajivprab.sava.database;

import com.google.common.collect.ImmutableSet;
import org.apache.commons.lang3.Validate;
import org.rajivprab.cava.exception.SQLExceptionc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Collection;

/**
 * Factory for creating new DB connections
 * <p>
 * Created by rprabhakar on 10/5/15.
 */
public class ConnectionFactory {
    private static final Collection<Integer> LEGAL_ISOLATION_LEVELS =
            ImmutableSet.of(Connection.TRANSACTION_NONE, Connection.TRANSACTION_READ_COMMITTED,
                            Connection.TRANSACTION_READ_UNCOMMITTED, Connection.TRANSACTION_REPEATABLE_READ,
                            Connection.TRANSACTION_SERIALIZABLE);

    private final int port, isolationLevel;
    private final String driver, host, database, user, password;

    public ConnectionFactory(String driver, int isolationLevel,
                             String host, int port, String database, String user, String password) {
        Validate.isTrue(LEGAL_ISOLATION_LEVELS.contains(isolationLevel));
        this.driver = driver;
        this.isolationLevel = isolationLevel;
        this.host = host;
        this.port = port;
        this.database = database;
        this.user = user;
        this.password = password;
    }

    public Connection createNewConnection() {
        try {
            Connection connection = DriverManager.getConnection(driver + "://" + host + ":" + port + "/" + database,
                                                                user, password);
            connection.setTransactionIsolation(isolationLevel);
            connection.setAutoCommit(true);
            return connection;
        } catch (SQLException e) {
            throw new SQLExceptionc(e);
        }
    }
}
