package org.rajivprab.sava.database;

import org.jooq.Converter;

import java.sql.Date;
import java.time.LocalDate;

/**
 * See: https://www.jooq.org/doc/3.7/manual/sql-building/queryparts/custom-bindings/
 *
 * Should no longer be needed with JOOQ 3.15, which offers native support for converting Date to java.time.LocalDate.
 * Remove this config from your jooq_generator.xml
 */
@Deprecated
public class JooqLocalDateConverter implements Converter<Date, LocalDate> {

    @Override
    public LocalDate from(Date t) {
        return t == null ? null : LocalDate.parse(t.toString());
    }

    @Override
    public Date to(LocalDate u) {
        return u == null ? null : Date.valueOf(u.toString());
    }

    @Override
    public Class<Date> fromType() {
        return Date.class;
    }

    @Override
    public Class<LocalDate> toType() {
        return LocalDate.class;
    }
}
