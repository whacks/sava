package org.rajivprab.sava.events;

import com.amazonaws.services.sns.AmazonSNS;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.rajivprab.cava.Validatec;

import java.time.Duration;
import java.time.Instant;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Implementation of publisher that uses SNS
 * <p>
 * Created by rajivprab on 6/4/17.
 */
class SNSPublisher implements Publisher {
    private static final Logger log = LogManager.getLogger(Publisher.class);

    private final AmazonSNS snsClient;
    private final Optional<String> reportErrorsToTopic;

    private final int maxPublishPerWindow;
    private final Duration rateLimitWindow;
    private final Map<String, AtomicInteger> topicPublishCount = Maps.newConcurrentMap();
    private final AtomicReference<Instant> timeWindowStart = new AtomicReference<>(Instant.now());

    static SNSPublisher def(AmazonSNS client) {
        Duration window = Duration.ofMinutes(10);
        // AWS enforces 10 emails/s per topic-subscription
        // But even that is too high. Default: cap it at 0.5/s => 300 emails, per topic-host, in a 10-minute window
        return new SNSPublisher(client, null, (int) window.toSeconds() / 2, window);
    }

    private SNSPublisher(AmazonSNS client, String reportErrorsToTopic,
                 int maxPublishPerWindow, Duration rateLimitWindow) {
        Validatec.greaterOrEqual(maxPublishPerWindow, 1);
        this.snsClient = client;
        this.reportErrorsToTopic = Optional.ofNullable(reportErrorsToTopic);
        this.maxPublishPerWindow = maxPublishPerWindow;
        this.rateLimitWindow = rateLimitWindow;
    }

    @Override
    public Publisher reportErrorsToTopic(String sendErrorTopic) {
        return new SNSPublisher(snsClient, Validatec.notEmpty(sendErrorTopic),
                                maxPublishPerWindow, rateLimitWindow);
    }

    @Override
    public Publisher rateLimitsPerTopic(int maxPublishPerWindow, Duration timeWindow) {
        return new SNSPublisher(snsClient, reportErrorsToTopic.orElse(null),
                                maxPublishPerWindow, timeWindow);
    }

    @Override
    public void publish(String topic, String title, String message) {
        try {
            if (!checkAndIncrementRateLimit(topic)) { return; }
            rawPublish(topic, title, message);
        } catch (RuntimeException e) {
            String originalMessage = String.format("Topic: %s, Title: %s, Message: %s", topic, title, message);
            log.error("Unable to publish on SNS. " + originalMessage, e);
            String errorTopic = reportErrorsToTopic.orElseThrow(() -> e);
            try {
                rawPublish(errorTopic, "SNS Publish Error",
                           "Original message:\n" + originalMessage + "\n" + ExceptionUtils.getStackTrace(e));
            } catch (RuntimeException e2) {
                log.fatal("Error reporting SNS-publish failed as well", e2);
                throw e2;
            }
        }
    }

    // -------------------- Helper --------------------

    private void rawPublish(String topic, String title, String message) {
        title = title.length() > 99 ? title.substring(0, 90) : title;
        title = title.trim().length() < 5 ? title + "_____" : title;
        title = title.replaceAll("[^A-Za-z0-9 ]", "_");
        snsClient.publish(topic, message, title);
    }

    // Uses best-effort. Time window and counts may be slightly off due to concurrency
    private boolean checkAndIncrementRateLimit(String topic) {
        var windowStart = timeWindowStart.get();
        if (windowStart.plus(rateLimitWindow).isBefore(Instant.now())) {
            // Start new window
            boolean updated = timeWindowStart.compareAndSet(windowStart, Instant.now());
            if (updated) {
                log.info("Resetting time window. Clearing counts");
                topicPublishCount.clear();
            }
        }
        int numPublishedAlready = topicPublishCount.computeIfAbsent(topic, k -> new AtomicInteger(0))
                                                   .getAndIncrement();
        // Only publish if equal. Otherwise, this will defeat the purpose of rate limiting by sending a warning
        // message for every message that is dropped
        if (numPublishedAlready == maxPublishPerWindow) {
            var message = "Hit SNSPublisher rate limit. Messages will be dropped till the next time window";
            rawPublish(topic,message, message);
        }

        return numPublishedAlready < maxPublishPerWindow;
    }
}
