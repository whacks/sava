package org.rajivprab.sava.session;

import java.io.Serializable;
import java.time.Duration;
import java.time.Instant;

/**
 * Representation of a user-session, consisting of relevant user/session data, along with an expiry.
 * Intended to be used together with SessionParser, which will reversibly convert a Session into a String token.
 */
public class Session<T> implements Serializable {
    private final T data;
    private final Instant expiry;

    public static <T> Session<T> build(T data, Duration duration) {
        return build(data, Instant.now().plus(duration));
    }

    public static <T> Session<T> build(T data, Instant expiry) {
        return new Session<>(data, expiry);
    }

    private Session(T data, Instant expiry) {
        this.data = data;
        this.expiry = expiry;
    }

    public Instant getExpiry() { return expiry; }

    public T getData() { return data; }

    // ------------ Object defaults -------------

    @Override
    public String toString() {
        return "Session{" +
                "sessionData=" + data +
                ", sessionExpiry=" + expiry +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Session that = (Session) o;

        return data.equals(that.data) && expiry.equals(that.expiry);
    }

    @Override
    public int hashCode() {
        return 31 * data.hashCode() + expiry.hashCode();
    }
}
