package org.rajivprab.sava.session;

import org.rajivprab.sava.login.Crypto;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.util.Arrays;

public interface SessionMac {
    // Depending on implementation, Mac may not be thread-safe. Do not try to reuse one instance.
    // Call this method each time
    Mac build();

    static SessionMac factoryHmacSha1(byte[] key) {
        return factory(key, "HmacSHA1");
    }

    static SessionMac factoryHmacSha256(byte[] key) {
        return factory(key, "HmacSHA256");
    }

    static SessionMac factory(byte[] key, String algorithm) {
        return new SessionHmac(key, algorithm);
    }

    class SessionHmac implements SessionMac {
        byte[] key;
        String algorithm;

        private SessionHmac(byte[] key, String algorithm) {
            this.key = Arrays.copyOf(key, key.length);
            this.algorithm = algorithm;
        }

        @Override
        public Mac build() {
            SecretKeySpec hmacSigningKey = new SecretKeySpec(key, algorithm);
            return Crypto.getMacInstance(algorithm, hmacSigningKey);
        }
    }
}
