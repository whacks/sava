package org.rajivprab.sava.push_notifications;

import com.turo.pushy.apns.ApnsClient;
import com.turo.pushy.apns.ApnsClientBuilder;
import com.turo.pushy.apns.PushNotificationResponse;
import com.turo.pushy.apns.auth.ApnsSigningKey;
import com.turo.pushy.apns.util.ApnsPayloadBuilder;
import com.turo.pushy.apns.util.SimpleApnsPushNotification;
import com.turo.pushy.apns.util.TokenUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.rajivprab.cava.ThreadUtilc;
import org.rajivprab.cava.exception.CheckedExceptionWrapper;
import org.rajivprab.cava.exception.IOExceptionc;
import org.rajivprab.sava.logging.LogDispatcher;
import org.rajivprab.sava.logging.Severity;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.Future;

/**
 * See README on https://github.com/relayrides/pushy
 *
 * Not tested currently. Use at your own risk!
 */
class Apns {
    private static final Logger log = LogManager.getLogger(Apns.class);

    // TODO Fill in arguments for ApnsClient
    private final ApnsClient client = getTokenAuthenticationClient(null, null, null);
    private final LogDispatcher logDispatcher;

    public Apns(LogDispatcher logDispatcher) {
        this.logDispatcher = logDispatcher;
        Runtime.getRuntime().addShutdownHook(new Thread(this::shutdown));
    }

    // Example: buildNotification("efc7492bdbd8209", "com.example.myApp", "Example!")
    public Future<PushNotificationResponse<SimpleApnsPushNotification>> push(
            String deviceToken, String topic, String alertBody) {
        return client.sendNotification(buildNotification(deviceToken, topic, alertBody));
    }

    public void process(Future<PushNotificationResponse<SimpleApnsPushNotification>> responseFuture) {
        try {
            PushNotificationResponse<SimpleApnsPushNotification> response = ThreadUtilc.get(responseFuture);
            if (!response.isAccepted()) {
                logDispatcher.report(this, Severity.FATAL, "Notification rejected by the APNs gateway: " + response);
            }
        } catch (Exception e) {
            // TODO Only retry on specific exceptions, caused by temporary failures
            logDispatcher.report(this, Severity.FATAL, "Unknown error when sending APNS notification", e);
        }
    }

    // ------------------------- Helpers -------------------------

    private SimpleApnsPushNotification buildNotification(String deviceToken, String topic, String alertBody) {
        String payload = new ApnsPayloadBuilder().setAlertBody(alertBody).buildWithDefaultMaximumLength();
        return new SimpleApnsPushNotification(TokenUtil.sanitizeTokenString(deviceToken), topic, payload);
    }


    private ApnsClient getTlsAuthenticationClient(File p12File, String p12Password) {
        try {
            return new ApnsClientBuilder()
                    .setClientCredentials(p12File, p12Password)
                    .build();
        } catch (IOException e) {
            throw new IOExceptionc(e);
        }
    }

    private static ApnsClient getTokenAuthenticationClient(File pkcs8File, String teamId, String keyId) {
        try {
            return new ApnsClientBuilder()
                    .setSigningKey(ApnsSigningKey.loadFromPkcs8File(pkcs8File, teamId, keyId))
                    .build();
        } catch (Exception e) {
            throw CheckedExceptionWrapper.wrapIfNeeded(e);
        }
    }

    private void shutdown() {
        Future<Void> closeFuture = client.close();
        try {
            ThreadUtilc.get(closeFuture);
            log.info("Successfully shut-down the APNS client");
        } catch (Throwable e) {
            // TODO Enhancement: What if Threader has shutdown by this point?
            logDispatcher.report(Apns.class, Severity.ERROR, "Error while shutting down APNS client", e);
        }
    }
}
