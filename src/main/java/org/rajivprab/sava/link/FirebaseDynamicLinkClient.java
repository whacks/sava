package org.rajivprab.sava.link;

import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.apache.ApacheHttpTransport;
import com.google.api.client.json.jackson.JacksonFactory;
import com.google.api.services.firebasedynamiclinks.v1.FirebaseDynamicLinks;
import com.google.api.services.firebasedynamiclinks.v1.model.*;
import com.google.common.collect.Maps;
import org.rajivprab.cava.Validatec;
import org.rajivprab.cava.exception.IOExceptionc;
import org.rajivprab.sava.logging.Dispatcher;
import org.rajivprab.sava.logging.LogDispatcher;
import org.rajivprab.sava.logging.Severity;
import org.rajivprab.sava.rest.IllegalUserInputException;

import java.io.IOException;
import java.util.Map;

public class FirebaseDynamicLinkClient {
    // TODO Enhancement: Use Cache data-structure with maximum size, to prevent heap explosion
    private final Map<String, String> cache = Maps.newConcurrentMap();

    private final String desktopLandingPage;
    private final String applicationName;
    private final String apiKey;
    private final String dynamicLinkDomain;
    private final String androidPackageName;
    private final String iosBundleId;
    private final String ipadBundleId;
    private final String iosAppStoreId;
    private final LogDispatcher logDispatcher;

    private FirebaseDynamicLinkClient(String desktopLandingPage, String applicationName, String apiKey,
                                      String dynamicLinkDomain, String androidPackageName, String iosBundleId,
                                      String ipadBundleId, String iosAppStoreId, LogDispatcher logDispatcher) {
        this.desktopLandingPage = desktopLandingPage;
        this.applicationName = applicationName;
        this.apiKey = apiKey;
        this.dynamicLinkDomain = dynamicLinkDomain;
        this.androidPackageName = androidPackageName;
        this.iosBundleId = iosBundleId;
        this.ipadBundleId = ipadBundleId;
        this.iosAppStoreId = iosAppStoreId;
        this.logDispatcher = logDispatcher;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String createLink(String deepLink) {
        return cache.computeIfAbsent(deepLink, this::generateRaw);
    }

    private String generateRaw(String deepLink) {
        try {
            // new FirebaseDynamicLinksRequestInitializer();    // Not needed?
            CreateShortDynamicLinkResponse response = buildClient().shortLinks().create(generateRequest(deepLink))
                                                          .setKey(apiKey).execute();
            // TODO Validatec.isNull(response.getWarning());
            if (response.getWarning() != null) {
                logDispatcher.report(this, Severity.WARN, "CreateLink Warnings: " + response.getWarning());
            }
            return response.getShortLink();
        } catch (GoogleJsonResponseException e) {
            Validatec.equals(e.getDetails().getCode(), 400, () -> new IOExceptionc(e));    // 400 == Bad request
            logDispatcher.report(this, Severity.WARN, "CreateLink Bad-Request for argument: " + deepLink, e);
            throw new IllegalUserInputException(e.getDetails().getMessage());
        } catch (IOException e) {
            throw new IOExceptionc(e);
        }
    }

    private FirebaseDynamicLinks buildClient() {
        return new FirebaseDynamicLinks.Builder(new ApacheHttpTransport(), new JacksonFactory(), null)
                .setApplicationName(applicationName)
                .setRootUrl("https://firebasedynamiclinks.googleapis.com/")  // Will fail without this!!
                .build();
    }

    private CreateShortDynamicLinkRequest generateRequest(String link) {
        AndroidInfo androidInfo = new AndroidInfo().setAndroidPackageName(androidPackageName);
        IosInfo iosInfo = new IosInfo()
                .setIosAppStoreId(iosAppStoreId)
                .setIosIpadBundleId(ipadBundleId)
                .setIosBundleId(iosBundleId);
        DesktopInfo desktopInfo = new DesktopInfo().setDesktopFallbackLink(desktopLandingPage);
        DynamicLinkInfo info = new DynamicLinkInfo()
                .setDynamicLinkDomain(dynamicLinkDomain)
                .setLink(link)
                // TODO Enhancement: set SocialMetaTagInfo, to enable descriptions when sharing on social media
                .setAndroidInfo(androidInfo)
                .setIosInfo(iosInfo)
                .setDesktopInfo(desktopInfo);
        Suffix suffix = new Suffix().setOption("SHORT");
        return new CreateShortDynamicLinkRequest().setDynamicLinkInfo(info).setSuffix(suffix);
    }

    public static class Builder {
        private String desktopLandingPage;
        private String applicationName;
        private String apiKey;
        private String dynamicLinkDomain;
        private String androidPackageName;
        private String iosBundleId;
        private String ipadBundleId;
        private String iosAppStoreId;
        private LogDispatcher logDispatcher = LogDispatcher.build(Dispatcher.getDummyDispatcher());

        private Builder() {}

        public Builder setDesktopLandingPage(String desktopLandingPage) {
            this.desktopLandingPage = desktopLandingPage;
            return this;
        }

        public Builder setApplicationName(String applicationName) {
            this.applicationName = applicationName;
            return this;
        }

        public Builder setApiKey(String apiKey) {
            this.apiKey = apiKey;
            return this;
        }

        public Builder setDynamicLinkDomain(String dynamicLinkDomain) {
            this.dynamicLinkDomain = dynamicLinkDomain;
            return this;
        }

        public Builder setAndroidPackageName(String androidPackageName) {
            this.androidPackageName = androidPackageName;
            return this;
        }

        public Builder setIosBundleId(String iosBundleId) {
            this.iosBundleId = iosBundleId;
            if (this.ipadBundleId == null) {
                // TODO Enhancement: Does the following make sense?
                // this.ipadBundleId = iosBundleId;
            }
            return this;
        }

        public Builder setIpadBundleId(String ipadBundleId) {
            this.ipadBundleId = ipadBundleId;
            return this;
        }

        public Builder setIosAppStoreId(String iosAppStoreId) {
            this.iosAppStoreId = iosAppStoreId;
            return this;
        }

        public Builder setLogDispatcher(LogDispatcher logDispatcher) {
            this.logDispatcher = logDispatcher;
            return this;
        }

        public FirebaseDynamicLinkClient build() {
            return new FirebaseDynamicLinkClient(
                    desktopLandingPage, applicationName, apiKey, dynamicLinkDomain, androidPackageName,
                    iosBundleId, ipadBundleId, iosAppStoreId, logDispatcher);
        }
    }
}
