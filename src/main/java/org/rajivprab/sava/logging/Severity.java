package org.rajivprab.sava.logging;

// General guidelines: https://www.papertrail.com/solution/tips/logging-in-java-best-practices-and-tips/
//
// Our custom policy:
//
// NOTIFY: Everything is working as expected. But a significant event has occurred that may warrant alerting
// someone. Example: very large purchase has been made. Or a daily batch job has completed running
//
// WARN: User error that we expect to see happen periodically. But is cause for alarm if it suddenly spikes in
// frequency. Hence, we want to provide some operational visibility. Example: incorrect password entered
//
// CLIENT_BUG: API is behaving as expected, but has received an input that its client should have prevented from
// occurring. Example: user attempted to enter an invalid Year/make/Model, even though the client should have
// constrained the user's selections
//
// ERROR: API is not behaving as expected. But the bug's impact is known, and contained. Example: Not all requested
// database connections have been created, only a partial number
//
// FATAL: API is not behaving as expected. The bug is either unknown, or is known to have a very significant impact
// on the app, deserving of immediate debugging. Example: Unable to create database connections. Or encountered
// a NullPointerException from completely mysterious origins
public enum Severity {
    TRACE, DEBUG, INFO, NOTIFY, WARN, CLIENT_BUG, ERROR, FATAL
}
