package org.rajivprab.sava.logging;

import com.google.common.collect.*;

import java.util.Map;
import java.util.Set;

// Very simple implementation, suitable for testing purposes. Thread-safe. Does not make defensive copies.
public class BufferedDispatcher implements Dispatcher {
    private final Table<Severity, String, String> dispatches = Tables.synchronizedTable(HashBasedTable.create());
    private Set<Severity> ignore = ImmutableSet.of();

    public BufferedDispatcher ignore(Severity... severities) {
        ignore = ImmutableSet.copyOf(severities);
        return this;
    }

    @Override
    public void dispatch(Severity severity, String title, String message) {
        if (!ignore.contains(severity)) {
            dispatches.put(severity, title, message);
        }
    }

    // Returns an immutable snapshot
    public Map<String, String> getDispatches(Severity severity) {
        Map<String, String> result = dispatches.row(severity);
        // As per javadocs in Tables.synchronizedTable
        synchronized (dispatches) {
            return ImmutableMap.copyOf(result);
        }
    }

    // Returns an immutable snapshot
    public Table<Severity, String, String> getDispatches() {
        // As per javadocs in Tables.synchronizedTable
        synchronized (dispatches) {
            return ImmutableTable.copyOf(dispatches);
        }
    }

    public void clear() {
        dispatches.clear();
    }
}
