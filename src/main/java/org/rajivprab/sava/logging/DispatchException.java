package org.rajivprab.sava.logging;

/**
 * An exception which can be reported through the LogDispatcher
 * TODO Rename class to LoggableException
 *
 * Created by rprabhakar on 6/12/16.
 */
public class DispatchException extends RuntimeException implements Loggable {
    private final Severity severity;
    private final String message;
    private final Throwable cause;

    public DispatchException(Severity severity, String message) {
        this(severity, message, null);
    }

    public DispatchException(Severity severity, String message, Throwable cause) {
        this.severity = severity;
        this.message = message;
        this.cause = cause;
    }

    @Override
    public String logFile() {
        return getClass().getName();
    }

    @Override
    public String message() {
        return message;
    }

    @Override
    public Severity severity() {
        return severity;
    }

    @Override
    public Throwable throwable() {
        return cause;
    }
}
