package org.rajivprab.sava.logging;

/**
 * Representation of a message, which can be logged and dispatched by a LogDispatcher
 */
public interface Loggable {
    String logFile();
    Severity severity();
    String message();
    Throwable throwable();
}
