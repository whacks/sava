package org.rajivprab.sava.rest;

import org.rajivprab.sava.logging.Severity;

import javax.ws.rs.core.Response.Status;

import static org.rajivprab.sava.rest.RestUtil.buildJson;
import static org.rajivprab.sava.rest.RestUtil.buildResponse;

/**
 * Extension of DispatchWebAppException, customized for things that the client code should have prevented from
 * happening. Example: user attempted to enter an invalid Year/make/Model, even though the client should have
 * constrained the user's selections
 */
public class ClientBugException extends LoggableWebAppException {
    public ClientBugException(String message) {
        this(message, null);
    }

    public ClientBugException(String message, Throwable cause) {
        this(Severity.CLIENT_BUG, message, cause);
    }

    public ClientBugException(Severity severity, String message, Throwable cause) {
        super(severity, message, cause, buildResponse(buildJson(message), Status.BAD_REQUEST));
    }
}
