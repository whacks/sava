package org.rajivprab.sava.rest;

import org.rajivprab.sava.logging.Loggable;
import org.rajivprab.sava.logging.Severity;

import javax.ws.rs.core.Response;

/**
 * An extension of WebApplicationException: can generate a REST response corresponding to the exception object
 * In addition, also logs the error using LogDispatcher, once logException is invoked
 *
 * Do not extend WebApplicationException (or pass Response to super),
 * because that causes ErrorMapper to stop catching this exception
 * https://stackoverflow.com/questions/29414041/exceptionmapper-for-webapplicationexceptions-thrown-with-entity
 * https://github.com/jersey/jersey/issues/3716
 *
 * Created by rajivprab on 6/5/17.
 */
public class LoggableWebAppException extends RuntimeException implements Loggable {
    private final Severity severity;
    private final Response response;

    // ----------- Constructors -----------

    public LoggableWebAppException(Severity severity, String message, Response response) {
        super(message);
        this.severity = severity;
        this.response = response;
    }

    public LoggableWebAppException(Severity severity, Throwable cause, Response response) {
        super(cause);
        this.severity = severity;
        this.response = response;
    }

    public LoggableWebAppException(Severity severity, String message, Throwable cause, Response response) {
        super(message, cause);
        this.severity = severity;
        this.response = response;
    }

    // ---------- Getters -------------

    public Response getResponse() {
        return response;
    }

    @Override
    public String logFile() {
        return getClass().getName();
    }

    @Override
    public String message() {
        return getMessage();
    }

    @Override
    public Severity severity() {
        return severity;
    }

    @Override
    public Throwable throwable() {
        return this;
    }

    @Override
    public String toString() {
        return super.toString() + ", response=" + response;
    }
}
