package org.rajivprab.sava.fcm;

import com.google.android.gcm.server.Result;

public interface Fcm {
    void push(PushNotification pushNotification);

    static Fcm build(String serverKey, ResultChecker checker) {
        return new FcmImpl(serverKey, checker);
    }

    interface ResultChecker {
        /**
         * Returns true if there were no errors.
         * See javadocs for {@link Result}, for hints on how to parse the result
         */
        boolean check(Result result, String device);
    }
}
