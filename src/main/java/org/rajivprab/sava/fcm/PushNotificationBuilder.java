package org.rajivprab.sava.fcm;

import com.google.common.collect.Sets;
import org.json.JSONObject;
import org.rajivprab.cava.Validatec;

import java.util.Collection;
import java.util.Set;

public class PushNotificationBuilder {
    private String title;
    private String body;
    private final JSONObject payload = new JSONObject();
    private final Set<String> registrationTokens = Sets.newHashSet();

    public PushNotificationBuilder deviceToken(Collection<String> deviceTokens) {
        registrationTokens.addAll(deviceTokens);
        return this;
    }

    public PushNotificationBuilder title(String title) {
        this.title = Validatec.notNull(title);
        return this;
    }

    public PushNotificationBuilder body(String body) {
        this.body = trim(Validatec.notNull(body));
        return this;
    }

    public PushNotificationBuilder payload(JSONObject meta) {
        for (String key : meta.keySet()) {
            payload(key, meta.get(key));
        }
        return this;
    }

    public PushNotificationBuilder payload(String key, Object value) {
        if (value instanceof String) {
            value = trim((String) value);
        }
        payload.put(key, value);
        return this;
    }

    private static String trim(String value) {
        if (value.length() > 1000) {
            // Entire payload can only be 4kB, or FCM will throw an error
            // Trim each field to 1kB, as a heuristic for avoiding the above
            // Long term, should extend this to just under 4kB, and check at construction time
            // https://stackoverflow.com/questions/31264206/size-of-notification-payload-in-gcm-fcm
            value = value.substring(0, 1000) + "...";
        }
        return value;
    }

    public PushNotification build() {
        return new PushNotification(title, body, payload, registrationTokens);
    }
}
