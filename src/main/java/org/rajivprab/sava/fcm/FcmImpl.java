package org.rajivprab.sava.fcm;

import com.google.android.gcm.server.*;
import com.google.android.gcm.server.Message.Builder;
import com.google.android.gcm.server.Message.Priority;
import com.google.common.collect.ImmutableList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.rajivprab.cava.Validatec;
import org.rajivprab.cava.exception.IOExceptionc;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.stream.IntStream;

// Copied from https://stackoverflow.com/questions/37426542/firebase-cloud-messaging-notification-from-java-instead-of-firebase-console
class FcmImpl implements Fcm {
    private static final Logger log = LogManager.getLogger(Fcm.class);

    private final ResultChecker checker;
    private final Sender sender;

    FcmImpl(String serverKey, ResultChecker checker) {
        this.sender = new FcmSender(serverKey);
        this.checker = checker;
    }

    @Override
    public void push(PushNotification pushNotification) {
        try {
            log.info("Sending push-notification: " + pushNotification);
            List<String> tokens = ImmutableList.copyOf(pushNotification.registrationTokens());
            if (tokens.isEmpty()) { return; }
            MulticastResult results = sender.send(buildMessage(pushNotification), tokens, 3);
            log.info("Push-notification send results: " + results);
            Validatec.size(results.getResults(), tokens.size());
            IntStream.range(0, tokens.size()).forEach(i -> checker.check(results.getResults().get(i), tokens.get(i)));
        } catch (IOException e) {
            throw new IOExceptionc(e);
        }
    }

    private static Message buildMessage(PushNotification pushNotification) {
        // https://firebase.google.com/docs/cloud-messaging/http-server-ref
        Notification notification = new Notification.Builder("icon???")
                .badge(-1)
                .title(pushNotification.title())
                .body(pushNotification.body())
                .sound("default")
                // .clickAction()
                // .tag(pushNotification.collapseKey())
                .build();

        Message message = new Builder()
                .notification(notification)
                // .collapseKey(pushNotification.collapseKey())
                .priority(Priority.HIGH)
                .addData("payload", pushNotification.payload().toString())
                .build();
        log.info("About to send FCM message: " + message);
        return message;
    }

    private static class FcmSender extends Sender {
        private static final String FCM_URL = "https://fcm.googleapis.com/fcm/send";

        private FcmSender(String serverKey) {
            super(serverKey);
        }

        @Override
        protected HttpURLConnection getConnection(String url) throws IOException {
            return (HttpURLConnection) new URL(FCM_URL).openConnection();
        }
    }
}
