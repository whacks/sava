package org.rajivprab.sava.login;

import org.rajivprab.cava.exception.CheckedExceptionWrapper;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.GeneralSecurityException;
import java.security.spec.KeySpec;

/**
 * Generic library for generating MAC and Encryption-Secrets.
 * I'm not a security expert. Just a very motivated guy who googles a lot. Use at your own risk!
 * <p>
 * Created by rprabhakar on 12/26/15.
 */
public class Crypto {
    // Not thread-safe. Do not reuse the same instance
    public static Mac getMacInstance(String hmacAlgorithm, SecretKeySpec hmacSigningKey) {
        try {
            Mac mac = Mac.getInstance(hmacAlgorithm);
            mac.init(hmacSigningKey);
            return mac;
        } catch (GeneralSecurityException e) {
            throw CheckedExceptionWrapper.wrap(e);
        }
    }

    public static SecretKey genEncryptionSecretKey(String paddingFormat, String password, byte[] salt,
                                                   int iterationCount, int keyNumBits, String encryptionFormat) {
        try {
            SecretKeyFactory factory = SecretKeyFactory.getInstance(paddingFormat);
            KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, iterationCount, keyNumBits);
            SecretKey tmp = factory.generateSecret(spec);
            return new SecretKeySpec(tmp.getEncoded(), encryptionFormat);
        } catch (GeneralSecurityException e) {
            throw CheckedExceptionWrapper.wrap(e);
        }
    }
}
