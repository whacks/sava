package org.rajivprab.sava.login;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.rajivprab.cava.Validatec;
import org.rajivprab.cava.exception.IOExceptionc;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.io.IOException;
import java.net.URI;

/**
 * TODO Enhancement: Switch to using SDK once stable
 * <p>
 * Created by rajivprab on 6/19/17.
 */
public class Okta {
    private static final Logger log = LogManager.getLogger(Okta.class);

    private final String orgUrl;
    private final String apiToken;
    private final String groupID;

    // Heavily WIP. Subject to breaking changes in future versions. Use at your own risk
    // Recommend using AuthFirebase instead
    @Deprecated
    public Okta(String orgUrl, String apiToken, String groupID) {
        this.orgUrl = orgUrl;
        this.apiToken = apiToken;
        this.groupID = groupID;
        // this.client = getClient(orgUrl, apiToken);
    }

    public OktaAccount createAccountWithNoEmail(String username, String password) {
        return createAccount(username, getFakeLogin(username), password);
    }

    public OktaAccount createAccount(String username, String email, String password) {
        return createAccount(username, email, password, username, "The Great");
    }

    public OktaAccount createAccount(String email, String password) {
        return createAccount(email, password, getUsername(email), "The Great");
    }

    public OktaAccount createAccount(String email, String password, String givenName, String surName) {
        return createAccount(getUsername(email), email, password, givenName, surName);
    }

    public OktaAccount createAccount(String username, String email, String password, String givenName, String surName) {
        // Okta only allows logins in the form of an email address
        // But many applications (eg Reddit) have logins in the form of nicknames
        // Solution: Create a fake email, using {username}@fakedomain.com, if the real email won't work
        Validatec.notEmpty(username, LoginException.class, LoginException.BLANK_FIELD);
        String loginEmail = email.startsWith(username + "@") ? email : getFakeLogin(username);
        return createAccountRaw(loginEmail, email, password, givenName, surName);
    }

    // Use this only if user wishes to have a username that's distinct from his email address
    // Else, provide only the email address and not the username
    private OktaAccount createAccountRaw(String loginEmail, String email, String password,
                                         String givenName, String surName) {
        // When using SDK, user keeps getting stuck in password-reset stage, and isn't able to login with password
        // User user = UserBuilder.instance().setEmail(email).setLogin(login).setFirstName(givenName)
        //                       .setLastName(surName).setPassword(password).buildAndCreate(client, false);
        // user.activate(false);
        Validatec.noneEmpty(() -> new LoginException(LoginException.BLANK_FIELD),
                            loginEmail, email, password, givenName, surName);
        JSONObject profile = new JSONObject().put("email", email)
                                             .put("login", loginEmail)
                                             .put("firstName", givenName)
                                             .put("lastName", surName);
        JSONObject credentials = new JSONObject()
                .put("password", new JSONObject().put("value", password))
                // TODO Confirm that the only way to activate this is to click on the link that is emailed to the user?
                // If someone is able to get to the password reset page, without clicking on the email link, we're hosed
                .put("recovery_question", new JSONObject()
                        .put("question", "Fill in the blank: Barack _____ won the Presidency in 2008")
                        .put("answer", "Obama"));
        JSONObject params = new JSONObject().put("profile", profile)
                                            .put("groupIds", new JSONArray().put(groupID))
                                            .put("credentials", credentials);
        URI uri = UriBuilder.fromPath(orgUrl + "/api/v1/users").queryParam("activate", "false").build();
        HttpResponse response = execute(new HttpPost(uri), params);
        JSONObject responseBody = getJsonObject(response);
        switch (response.getStatusLine().getStatusCode()) {
            case 200:
                OktaAccount account = OktaAccount.parseCreateResponse(responseBody);
                activate(account.getOktaID());
                log.info("Created user: " + account);
                return account;
            case 400:
                String errorCause = responseBody.getJSONArray("errorCauses").getJSONObject(0)
                                                .getString("errorSummary");
                throw new LoginException(errorCause);
            default:
                throw new IllegalStateException("Response: " + response + ", Body: " + responseBody);
        }
    }

    // Works with both username and loginEmail, but does not work with email-address (if it differs from login-email)
    public OktaAccount verifyAccount(String login, String rawPassword) {
        JSONObject params = new JSONObject().put("username", login).put("password", rawPassword);
        HttpPost post = new HttpPost(orgUrl + "/api/v1/authn");
        HttpResponse response = execute(post, params);
        JSONObject responseJson = getJsonObject(response);
        switch (response.getStatusLine().getStatusCode()) {
            case 200:
                return OktaAccount.parseVerifyResponse(responseJson);
            case 400:
                throw new LoginException(responseJson.getJSONArray("errorCauses").getJSONObject(0)
                                                     .getString("errorSummary"));
            case 401:
                Validatec.equals(responseJson.getString("errorCode"), "E0000004");
                throw new LoginException(responseJson.getString("errorSummary"));
            default:
                throw new LoginException(responseJson.toString(2));
        }
    }

    // TODO Enhancement - Use custom email
    // Works with both username and login-email, but does not work with email-address (if it differs from login-email)
    public void resetPassword(String login) {
        URI uri = UriBuilder
                .fromPath(orgUrl + "/api/v1/users/" + getUser(login).getOktaID() + "/credentials/forgot_password")
                .queryParam("sendEmail", "true").build();
        HttpResponse response = execute(new HttpPost(uri));
        Validatec.equals(response.getStatusLine().getStatusCode(), Response.Status.OK.getStatusCode());
    }

    // Works with both username and login-email, but does not work with email-address (if it differs from login-email)
    OktaAccount getUser(String login) {
        URI uri = UriBuilder.fromPath(orgUrl + "/api/v1/users/" + login).build();
        HttpResponse response = execute(new HttpGet(uri));
        JSONObject responseJson = getJsonObject(response);
        switch (response.getStatusLine().getStatusCode()) {
            case 200:
                return OktaAccount.parseGetUserResponse(responseJson);
            case 404:
                Validatec.equals(responseJson.getString("errorCode"), "E0000007");
                throw new LoginException(responseJson.getString("errorSummary") +
                                                 ". Try using your username, and not your email address.");
            default:
                throw new LoginException(responseJson.toString(2));
        }
    }

    JSONArray listGroups() {
        URI uri = UriBuilder.fromPath(orgUrl + "/api/v1/groups").build();
        HttpResponse response = execute(new HttpGet(uri));
        Validatec.equals(response.getStatusLine().getStatusCode(), Response.Status.OK.getStatusCode());
        return new JSONArray(getResponseString(response));
    }

    static String getFakeLogin(String username) {
        Validatec.notEmpty(LoginException.BLANK_FIELD, LoginException.class, username);
        Validatec.isTrue(!username.contains("@"), LoginException.class, LoginException.ILLEGAL_USERNAME_MESSAGE);
        // For compatibility reasons with existing users, do not change the fake literal below
        return username + "@emailnotprovided.local";
    }

    // --------- Helpers

    private JSONObject activate(String userID) {
        URI uri = UriBuilder.fromPath(orgUrl + "/api/v1/users/" + userID + "/lifecycle/activate")
                            .queryParam("sendEmail", "false").build();
        HttpResponse response = execute(new HttpPost(uri));
        Validatec.equals(response.getStatusLine().getStatusCode(), Response.Status.OK.getStatusCode());
        return getJsonObject(response);
    }

    private JSONObject assignToApplication(String userID, String userName, String password, String applicationID) {
        URI uri = UriBuilder.fromPath(orgUrl + "/api/v1/apps/" + applicationID + "/users").build();
        JSONObject credentials = new JSONObject().put("password", new JSONObject().put("value", password))
                                                 .put("userName", userName);
        JSONObject body = new JSONObject().put("id", userID).put("scope", "USER").put("credentials", credentials);
        HttpResponse response = execute(new HttpPost(uri), body);
        JSONObject responseBody = getJsonObject(response);
        Validatec.equals(response.getStatusLine().getStatusCode(), Response.Status.OK.getStatusCode(),
                         "Response: " + response + ", Body: " + responseBody);
        return responseBody;
    }

    private HttpResponse execute(HttpUriRequest request) {
        try {
            addHeaders(request);
            // Creating a single client and reusing it produces errors/timeouts when launching many requests
            return HttpClients.createDefault().execute(request);
        } catch (IOException e) {
            throw new IOExceptionc(e);
        }
    }

    private HttpResponse execute(HttpPost request, JSONObject body) {
        try {
            addHeaders(request);
            request.setEntity(new StringEntity(body.toString()));
            // LogDispatcher.report(this, Severity.INFO, "Body: " + body); // Will print out password in log
            return HttpClients.createDefault().execute(request);
        } catch (IOException e) {
            throw new IOExceptionc(e);
        }
    }

    private void addHeaders(HttpUriRequest request) {
        request.addHeader("Content-Type", MediaType.APPLICATION_JSON);
        request.addHeader("Accept", MediaType.APPLICATION_JSON);
        request.addHeader("Authorization", "SSWS " + apiToken);
        log.info("Request: " + request);
    }

    private static JSONObject getJsonObject(HttpResponse response) {
        return new JSONObject(getResponseString(response));
    }

    private static String getResponseString(HttpResponse response) {
        try {
            String responseString = EntityUtils.toString(response.getEntity());
            log.info("Response: " + response);
            log.info("ResponseString: " + responseString);
            return responseString;
        } catch (IOException e) {
            throw new IOExceptionc(e);
        }
    }

    private static String getUsername(String email) {
        Validatec.matches(email, ".+\\@.+\\..+", "Invalid email: " + email);
        return email.split("@")[0];
    }

    /*
    private static Client getClient(String orgUrl, String apiToken) {
        return Clients.builder()
                      .setOrgUrl(orgUrl)
                      .setClientCredentials(new TokenClientCredentials(apiToken))
                      .build();
    }
    */
}
