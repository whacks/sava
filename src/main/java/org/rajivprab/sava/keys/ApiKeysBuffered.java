package org.rajivprab.sava.keys;

import com.google.common.collect.Maps;

import java.util.Map;

public class ApiKeysBuffered implements ApiKeys {
    private final Map<String, String> keys = Maps.newConcurrentMap();

    public ApiKeysBuffered set(String key, String value) {
        keys.put(key, value);
        return this;
    }

    public ApiKeysBuffered set(Map<String, String> entries) {
        keys.putAll(entries);
        return this;
    }

    @Override
    public String getCredentials(String key) {
        return keys.get(key);
    }
}
