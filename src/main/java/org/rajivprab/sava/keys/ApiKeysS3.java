package org.rajivprab.sava.keys;

import org.rajivprab.sava.s3.S3Interface;

/**
 * Simple API-Keys implementation.
 * Is thread-safe, as long as the underlying S3Interface is thread-safe.
 *
 * Created by rprabhakar on 2/1/16.
 */
public class ApiKeysS3 implements ApiKeys {
    private final String bucket;
    private final S3Interface s3Interface;

    public static ApiKeys build(String bucket, S3Interface s3Interface) {
        return new ApiKeysS3(bucket, s3Interface);
    }

    private ApiKeysS3(String bucket, S3Interface s3Interface) {
        this.bucket = bucket;
        this.s3Interface = s3Interface;
    }

    @Override
    public String getCredentials(String key) {
        return s3Interface.getFullContents(bucket, key);
    }
}
