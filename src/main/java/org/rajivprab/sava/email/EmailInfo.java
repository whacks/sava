package org.rajivprab.sava.email;

import com.google.api.client.util.Lists;
import com.google.common.collect.ImmutableList;
import com.google.common.net.MediaType;
import org.apache.commons.lang3.tuple.Pair;
import org.rajivprab.cava.Validatec;

import java.io.InputStream;
import java.util.List;
import java.util.Optional;

/**
 * Builder for the EmailInfo object, which is then used by the Email interface
 *
 * TODO Enhancement: Replace with Record once upgraded to Java 17
 *
 * Created by rprabhakar on 4/22/16.
 */
public class EmailInfo {
    private final String toEmail;
    private final String title;
    private final String body;
    private final String fromEmail;
    private final String replyToEmail;
    private final String toName;
    private final String fromName;
    private final int suppressionGroup;
    private final String bccEmail;
    private final List<Pair<String, String>> ccEmailName;
    private final List<Attachment> attachments;

    private EmailInfo(String toEmail, String title, String body, String fromEmail, String replyToEmail,
                      String toName, String fromName, int suppressionGroup,
                      String bccEmail, List<Pair<String, String>> ccEmailName, List<Attachment> attachments) {
        Validatec.greaterThan(suppressionGroup, 0);
        this.suppressionGroup = suppressionGroup;
        this.toEmail = Validatec.notEmpty(toEmail);
        this.title = Validatec.notEmpty(title);
        this.body = Validatec.notEmpty(body);
        this.fromEmail = Validatec.notEmpty(fromEmail);
        this.replyToEmail = Validatec.notEmpty(replyToEmail);
        this.toName = Validatec.notNull(toName);
        this.fromName = Validatec.notNull(fromName);
        this.bccEmail = bccEmail;
        this.ccEmailName = ImmutableList.copyOf(ccEmailName);
        this.attachments = ImmutableList.copyOf(attachments);
    }

    // ------------- Getters -----------

    public String getToEmail() {
        return toEmail;
    }

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }

    public String getFromEmail() {
        return fromEmail;
    }

    public String getReplyToEmail() {
        return replyToEmail;
    }

    public String getToName() {
        return toName;
    }

    public String getFromName() {
        return fromName;
    }

    public int getSuppressionGroup() {
        return suppressionGroup;
    }

    public Optional<String> getBccEmail() {
        return Optional.ofNullable(bccEmail);
    }

    public List<Pair<String, String>> getCcEmailName() {
        return ImmutableList.copyOf(ccEmailName);
    }

    public List<Attachment> getAttachments() {
        return ImmutableList.copyOf(attachments);
    }

    // --------------

    @Override
    public String toString() {
        return "EmailInfo{" +
                "toEmail='" + toEmail + '\'' +
                ", title='" + title + '\'' +
                ", body='" + body + '\'' +
                ", fromEmail='" + fromEmail + '\'' +
                ", replyToEmail='" + replyToEmail + '\'' +
                ", toName='" + toName + '\'' +
                ", fromName='" + fromName + '\'' +
                ", suppressionGroup='" + suppressionGroup + '\'' +
                ", bccEmail='" + bccEmail + '\'' +
                ", cc='" + ccEmailName + '\'' +
                ", attachments='" + attachments + '\'' +
                '}';
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private String toEmail;
        private String title;
        private String body;
        private String fromEmail;
        private String replyToEmail;
        private int suppressionGroup;

        private String toName = "";
        private String fromName = "";
        private String bccEmail = null;
        private final List<Pair<String, String>> ccEmailName = Lists.newArrayList();
        private List<Attachment> attachments = Lists.newArrayList();

        public Builder setToEmail(String toEmail) {
            this.toEmail = toEmail;
            return this;
        }

        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder setBody(String body) {
            this.body = body;
            return this;
        }

        public Builder setFromEmail(String fromEmail) {
            this.fromEmail = fromEmail;
            if (replyToEmail == null) {
                setReplyToEmail(fromEmail);
            }
            return this;
        }

        public Builder setReplyToEmail(String replyToEmail) {
            this.replyToEmail = replyToEmail;
            return this;
        }

        public Builder setToName(String toName) {
            this.toName = toName;
            return this;
        }

        public Builder setFromName(String fromName) {
            this.fromName = fromName;
            return this;
        }

        public Builder setSuppressionGroup(int suppressionGroup) {
            this.suppressionGroup = suppressionGroup;
            return this;
        }

        public Builder setBccEmail(String bccEmail) {
            this.bccEmail = bccEmail;
            return this;
        }

        public Builder addCC(String email) {
            return addCC(email, null);
        }

        public Builder addCC(String email, String name) {
            this.ccEmailName.add(Pair.of(email, name));
            return this;
        }

        public Builder addAttachment(Attachment attachment) {
            this.attachments.add(attachment);
            return this;
        }

        public EmailInfo build() {
            return new EmailInfo(toEmail, title, body, fromEmail, replyToEmail,
                                 toName, fromName, suppressionGroup, bccEmail, ccEmailName, attachments);
        }
    }
}
