package org.rajivprab.sava.email;

import com.google.common.collect.ImmutableMap;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.ASM;
import com.sendgrid.helpers.mail.objects.Attachments;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;
import javax.ws.rs.core.MediaType;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.rajivprab.cava.IOUtilc;
import org.rajivprab.cava.Validatec;
import org.rajivprab.cava.exception.IOExceptionc;
import org.rajivprab.sava.email.Attachment.EmailAttachmentDisposition;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * SendGrid email client
 *
 * To generate suppression-groups: https://app.sendgrid.com/suppressions/advanced_suppression_manager
 * <p>
 * Created by rprabhakar on 3/21/16.
 */
public class SendGrids {
    private static final Logger log = LogManager.getLogger(SendGrids.class);

    private static final Map<EmailAttachmentDisposition, String> DISPOSITION_STRING_MAP = ImmutableMap.of(
            EmailAttachmentDisposition.ATTACHMENT, "attachment",
            EmailAttachmentDisposition.INLINE, "inline");

    private final String apiKey;
    private final SendGrid client;

    public static SendGrids client(String apiKey) {
        return new SendGrids(apiKey);
    }

    SendGrids(String apiKey) {
        this.apiKey = apiKey;
        this.client = new SendGrid(apiKey);
    }

    public void unsubscribe(String email, int suppressionGroup) {
        sendRequest(getUnsubReq(email, suppressionGroup));
    }

    public void resubscribe(String email, int suppressionGroup) {
        sendRequest(getResubReq(email, suppressionGroup));
    }

    public boolean sendEmail(EmailInfo info) {
        Email from = new Email(info.getFromEmail(), info.getFromName());
        Email to = new Email(info.getToEmail(), info.getToName());
        Mail mail = new Mail(from, info.getTitle(), to, new Content("text/html", info.getBody()));
        mail.setReplyTo(new Email(info.getReplyToEmail()));
        info.getBccEmail().ifPresent(bcc -> mail.getPersonalization().get(0).addBcc(new Email(bcc)));

        for (var attachment : info.getAttachments()) {
            Attachments sendGridAttachment = new Attachments.Builder(attachment.fileName, attachment.content)
                    .withType(attachment.type.toString())
                    .withDisposition(DISPOSITION_STRING_MAP.get(attachment.disposition))
                    .build();
            mail.addAttachments(sendGridAttachment);
        }

        info.getCcEmailName().forEach(emailName -> {
            if (StringUtils.isBlank(emailName.getRight())) {
                mail.getPersonalization().get(0).addCc(new Email(emailName.getLeft()));
            } else {
                mail.getPersonalization().get(0).addCc(new Email(emailName.getLeft(), emailName.getRight()));
            }
        });

        setSubAccount(mail, info.getSuppressionGroup());

        log.info("Sending email: " + info);
        try {
            Request request = new Request();
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            Response response = client.api(request);

            log.info("Email send status: " + response.getBody());
            Validatec.equals(response.getStatusCode(), 202, () -> new SendGridException(response));
            return true;
        } catch (IOException e) {
            log.error("Sendgrid error when sending: " + info, e);
            throw new IOExceptionc(e);
        }
    }

    // ----------------------------------

    private void setSubAccount(Mail mail, int suppressionGroup) {
        ASM asm = new ASM();
        asm.setGroupId(suppressionGroup);
        mail.setASM(asm);
        mail.addCategory("Suppression Group: " + suppressionGroup);
    }

    private HttpUriRequest getUnsubReq(String emailAddress, int suppressionGroup) {
        HttpPost post = new HttpPost(getBaseURI(suppressionGroup));
        post.setEntity(getRequestEntity(emailAddress));
        return post;
    }

    private HttpUriRequest getResubReq(String emailAddress, int suppressionGroup) {
        String uri = getBaseURI(suppressionGroup) + "/" + emailAddress;
        return new HttpDelete(uri);
    }

    private String getBaseURI(int suppressionGroup) {
        return "https://api.sendgrid.com/v3/asm/groups/" + suppressionGroup + "/suppressions";
    }

    private void sendRequest(HttpUriRequest req) {
        try {
            req.setHeader("Authorization", "Bearer " + apiKey);
            log.info("SendGrid request: " + req);
            // Reusing a single HttpClients produces errors/timeouts when sending many requests
            HttpResponse response = HttpClients.createDefault().execute(req);
            int responseStatus = parseResponse(response);
            Validatec.isTrue(responseStatus >= 200 && responseStatus <= 299,
                             "Sendgrid Failure code: " + responseStatus + ", see earlier resp log");
        } catch (IOException e) {
            throw new IOExceptionc(e);
        }
    }

    private static int parseResponse(HttpResponse response) throws IOException {
        String responseEntity = "";
        try {
            responseEntity = IOUtilc.toString(response.getEntity().getContent());
        } catch (NullPointerException ignored) {}
        int responseStatus = response.getStatusLine().getStatusCode();
        log.info("SendGrid Response code: {}, entity: {}\nFull response: {}",
                 responseStatus, responseEntity, "" + response);
        return responseStatus;
    }

    private static StringEntity getRequestEntity(String... emailAddresses) {
        try {
            JSONArray emails = new JSONArray();
            for (String email : emailAddresses) {
                emails.put(email);
            }
            JSONObject json = new JSONObject();
            json.put("recipient_emails", emails);
            log.info("SendGrid request entity: " + json);
            StringEntity entity = new StringEntity(json.toString());
            entity.setContentType(MediaType.APPLICATION_JSON);
            return entity;
        } catch (UnsupportedEncodingException e) {
            throw new IOExceptionc(e);
        }
    }
}
