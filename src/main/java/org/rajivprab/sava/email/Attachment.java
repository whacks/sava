package org.rajivprab.sava.email;

import com.google.common.net.MediaType;

import java.io.InputStream;

public class Attachment {
    // https://docs.sendgrid.com/api-reference/mail-send/mail-send
    public enum EmailAttachmentDisposition {INLINE, ATTACHMENT}

    final MediaType type;
    final InputStream content;
    final String fileName;
    final EmailAttachmentDisposition disposition;

    public static Attachment build(MediaType type,
                                   InputStream content,
                                   String fileName,
                                   EmailAttachmentDisposition disposition) {
        return new Attachment(type, content, fileName, disposition);
    }

    private Attachment(MediaType type,
                       InputStream content,
                       String fileName,
                       EmailAttachmentDisposition disposition) {
        this.type = type;
        this.content = content;
        this.fileName = fileName;
        this.disposition = disposition;
    }
}
