package org.rajivprab.sava.email;

import com.sendgrid.Response;

// Error documentation is here: https://sendgrid.com/docs/API_Reference/Web_API_v3/Mail/errors.html
public class SendGridException extends RuntimeException {
    private final Response response;

    SendGridException(Response response) {
        this.response = response;
    }

    public Response getResponse() {
        return response;
    }
}
