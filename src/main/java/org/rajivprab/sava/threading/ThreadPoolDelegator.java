package org.rajivprab.sava.threading;

import org.eclipse.jetty.util.thread.ThreadPool;

/**
 * Enables defining a new ThreadPool which is a thin wrapper around another ThreadPool,
 * except for a few overridden methods.
 *
 * Ie, composition over inheritance.
 *
 * Example usage: see {@link CapacityLoggingThreadPool}
 */
public interface ThreadPoolDelegator extends ThreadPool {
    ThreadPool getDelegate();

    @Override
    default void execute(Runnable command) {
        getDelegate().execute(command);
    }

    @Override
    default void join() throws InterruptedException {
        getDelegate().join();
    }

    @Override
    default int getThreads() {
        return getDelegate().getThreads();
    }

    @Override
    default int getIdleThreads() {
        return getDelegate().getIdleThreads();
    }

    @Override
    default boolean isLowOnThreads() {
        return getDelegate().isLowOnThreads();
    }
}
