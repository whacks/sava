package org.rajivprab.sava.threading;

import com.google.common.collect.Queues;
import org.apache.commons.lang3.Validate;
import org.rajivprab.cava.ThreadUtilc;
import org.rajivprab.cava.Validatec;
import org.rajivprab.cava.delegator.ExecutorServiceDelegator;
import org.rajivprab.cava.exception.CheckedExceptionWrapper;
import org.rajivprab.cava.exception.InterruptedExceptionc;
import org.rajivprab.sava.logging.Dispatcher;
import org.rajivprab.sava.logging.LogDispatcher;
import org.rajivprab.sava.logging.Severity;

import javax.annotation.Nonnull;
import java.time.Duration;
import java.util.Queue;
import java.util.concurrent.*;

/**
 * Interface for launching of background threads
 * QoS not guaranteed. Do not use for time-sensitive operations
 *
 * Thin wrapper around the underlying ExecutorService.
 *
 * Enables single flag-flip, after which any future tasks given will be tracked.
 * Status of all tracked tasks can then be tracked using checkTasks.
 * This is very useful for testing purposes,
 * where we want to ensure that all background tasks have completed without errors.
 *
 * Also runs all commands on the main thread, if execution is rejected for any reason.
 * This degrades performance, but prevents user errors.
 *
 * <p>
 * TODO Enhancement: What happens if shutdown is invoked while threads are in flight?
 * They will get killed part-way through?
 * Does Heroku guarantee that if you try to reboot a dyno,
 * it will not send any requests to that dyno for short period before?
 * <p>
 * Created by rprabhakar on 7/11/15.
 */
public class ThreadHelper implements ExecutorServiceDelegator, AutoCloseable {
    private final ExecutorService executor;
    private final LogDispatcher logDispatcher;
    private final Queue<Future> trackedFutures;

    public static ThreadHelper build(ExecutorService executor) {
        return build(executor, false);
    }

    public static ThreadHelper build(ExecutorService executor, LogDispatcher logDispatcher) {
        return new ThreadHelper(executor, false, logDispatcher);
    }

    public static ThreadHelper build(ExecutorService executor, boolean trackTasks) {
        return new ThreadHelper(executor, trackTasks, LogDispatcher.build(Dispatcher.getWarningDispatcher()));
    }

    private ThreadHelper(ExecutorService executor, boolean trackTasks, LogDispatcher logDispatcher) {
        this.executor = Validatec.notNull(executor);
        this.logDispatcher = Validatec.notNull(logDispatcher);
        this.trackedFutures = trackTasks ? Queues.newConcurrentLinkedQueue() : null;
    }

    // -------------------- Public Functionality --------------------------

    // If any exceptions are hit while running command:
    //      if trackTasks has been set, the exception can only be thrown in main thread through checkTasks.
    //      else if using ThreadPoolExecutor, executor.afterExecute will be called on external thread prior to death.
    @Override
    public void execute(@Nonnull Runnable runnable) {
        if (isTrackingTasks()) {
            Future<Object> future = submit(runnable);
            boolean success = trackedFutures.add(future);
            Validate.isTrue(success, "Unable to add future to tracker-queue");
        } else {
            try {
                getDelegate().execute(runnable);
            } catch (RejectedExecutionException e) {
                runnable.run();
                // Only report error after finishing main task
                logDispatcher.report(this, Severity.ERROR,
                                     "Execution rejected. Shutdown invoked? Running on main thread", e);
            }
        }
    }

    // The Future's {@code get} method will return {@code null} upon successful completion.
    // All comments below for submit(Callable) apply here as well
    @Override
    public Future<Object> submit(Runnable runnable) {
        Callable<Object> callable = () -> {
            runnable.run();
            return null;
        };
        return submit(callable);
    }

    // If any exceptions are hit while running command, the exception can only be triggered in main thread by
    // calling Future.get. checkTasks() will not do anything.
    // Can't rely on executor.afterExecute to do anything useful, since submit is called and not execute.
    // If main thread is not calling Future.get, use execute instead of submit.
    @Override
    public <T> Future<T> submit(Callable<T> callable) {
        try {
            // Do not track futures here, even if trackTasks is set. It is very confusing when the same
            // exception is thrown twice. First in the main thread via future.get(), and again via checkTasks().
            //
            // If the caller is not using future.get(), they should instead use execute,
            // which will respect checkTasks()
            return getDelegate().submit(callable);
        } catch (RejectedExecutionException e) {
            try {
                Future<T> future = CompletableFuture.completedFuture(callable.call());
                logDispatcher.report(this, Severity.ERROR,
                                     "Execution rejected. Shutdown invoked? Running on main thread", e);
                return future;
            } catch (Exception ex) {
                throw CheckedExceptionWrapper.wrapIfNeeded(ex);
            }
        }
    }

    @Override
    public void close() {
        shutdown();
        Validate.isTrue(isShutdown());
        Validate.isTrue(awaitTermination(Duration.ofDays(365)));
        Validate.isTrue(isTerminated());
    }

    public boolean isTrackingTasks() {
        return trackedFutures != null;
    }

    public void checkTasks() {
        Validate.isTrue(isTrackingTasks(), "checkTasks called without trackTasks being set");
        Future future = trackedFutures.poll();
        while (future != null) {
            ThreadUtilc.get(future);
            future = trackedFutures.poll();
        }
    }

    public boolean awaitTermination(Duration timeout) {
        try {
            return getDelegate().awaitTermination(timeout.toMillis(), TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            throw new InterruptedExceptionc(e);
        }
    }

    @Override
    public ExecutorService getDelegate() {
        return executor;
    }
}
