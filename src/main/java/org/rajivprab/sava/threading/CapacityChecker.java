package org.rajivprab.sava.threading;

import org.apache.commons.lang3.Validate;

import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.atomic.AtomicReference;

abstract class CapacityChecker {
    private static final Duration MAX_ALERT_FREQUENCY = Duration.ofMinutes(60);
    private static final Duration MAX_CHECK_FREQUENCY = Duration.ofMinutes(1);

    private final AtomicReference<Instant> lastAlert = new AtomicReference<>(Instant.MIN);
    private final AtomicReference<Instant> lastCheck = new AtomicReference<>(Instant.MIN);

    protected abstract boolean atLowCapacity();
    protected abstract void fireAlert();

    public void checkCapacity() {
        // Checking capacity on a ThreadPool will require multiple atomic/volatile operations. Hence it is faster
        // to simply do this one volatile get
        Instant lastCheck = this.lastCheck.get();
        if (eventOccurredRecently(lastCheck, MAX_CHECK_FREQUENCY)) { return; }

        // Use compareAndSet to ensure that only one thread does the check, alert and updating of instance variables
        if (this.lastCheck.compareAndSet(lastCheck, Instant.now())) {
            Instant lastAlert = this.lastAlert.get();
            if (eventOccurredRecently(lastAlert, MAX_ALERT_FREQUENCY)) { return; }
            if (atLowCapacity()) {
                boolean updated = this.lastAlert.compareAndSet(lastAlert, Instant.now());
                Validate.isTrue(updated, "There should only be one thread in this critical section");
                fireAlert();
            }
        }
    }

    // ---------------

    private static boolean eventOccurredRecently(Instant lastEvent, Duration maxFrequency) {
        return lastEvent.isAfter(Instant.now().minus(maxFrequency));
    }
}
