package org.rajivprab.sava.threading;

import com.google.common.collect.Queues;
import org.rajivprab.sava.logging.LogDispatcher;
import org.rajivprab.sava.logging.Loggable;
import org.rajivprab.sava.logging.Severity;

import java.util.concurrent.*;

/**
 * {@link ThreadPoolExecutor} that additionally logs throwables that the thread encounters
 * More info: http://stackoverflow.com/questions/1800317/impossible-to-make-a-cached-thread-pool-with-a-size-limit
 *
 * Has to be a ThreadPoolExecutor in order to override {@link ThreadPoolExecutor#afterExecute(Runnable, Throwable)}
 * <p>
 * Created by rprabhakar on 12/21/15.
 */
public class ExceptionReportingThreadPoolExecutor extends ThreadPoolExecutor {
    private final LogDispatcher logDispatcher;

    // -------------- Constructors --------------

    /** Builds a {@link Executors#newCachedThreadPool} */
    public static ThreadPoolExecutor build(LogDispatcher logDispatcher) {
        return build(0, Integer.MAX_VALUE, 60L, TimeUnit.SECONDS,
                     new SynchronousQueue<>(), logDispatcher);
    }

    /** Builds a {@link Executors#newFixedThreadPool} */
    public static ThreadPoolExecutor build(int fixedPoolSize, LogDispatcher logDispatcher) {
        return build(fixedPoolSize, fixedPoolSize, 0, TimeUnit.MILLISECONDS,
                     Queues.newLinkedBlockingQueue(), logDispatcher);
    }

    public static ThreadPoolExecutor build(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit,
                                           BlockingQueue<Runnable> workQueue, LogDispatcher logDispatcher) {
        return new ExceptionReportingThreadPoolExecutor(corePoolSize, maximumPoolSize, keepAliveTime, unit,
                                                        workQueue, logDispatcher);
    }

    private ExceptionReportingThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit,
                                                 BlockingQueue<Runnable> workQueue, LogDispatcher logDispatcher) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue);
        this.logDispatcher = logDispatcher;
    }

    // ----------------------------

    /**
     * See {@link ThreadPoolExecutor#afterExecute}
     *
     * Note: {@link ExecutorService#execute} will set throwable appropriately,
     * but {@link ExecutorService#submit} always sets throwable to NULL.
     * It instead returns a Future. Consumer should then call {@link Future#get} to check for exceptions.
     */
    @Override
    public void afterExecute(Runnable runnable, Throwable throwable) {
        super.afterExecute(runnable, throwable);
        if (throwable != null) {
            if (throwable instanceof Loggable) {
                logDispatcher.report((Loggable) throwable);
            } else {
                logDispatcher.report(this, Severity.FATAL, throwable);
            }
        }
    }
}