package org.rajivprab.sava.threading;

import com.google.common.collect.ImmutableMap;
import org.rajivprab.cava.delegator.ExecutorServiceDelegator;
import org.rajivprab.sava.logging.LogDispatcher;
import org.rajivprab.sava.logging.Severity;

import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * ThreadPoolExecutor that also fires alerts when the thread-pool utilization becomes too high
 */
public class CapacityLoggingThreadPoolExecutor implements ExecutorServiceDelegator {
    private final ThreadPoolExecutor pool;
    private final CapacityChecker capacityChecker;

    public static ExecutorService build(ThreadPoolExecutor pool, LogDispatcher logDispatcher) {
        return new CapacityLoggingThreadPoolExecutor(pool, logDispatcher);
    }

    private CapacityLoggingThreadPoolExecutor(ThreadPoolExecutor pool, LogDispatcher logDispatcher) {
        this.pool = pool;
        this.capacityChecker = new ThreadPoolExecutorCapacityChecker(pool, logDispatcher);
    }

    @Override
    public ExecutorService getDelegate() {
        return pool;
    }

    @Override
    public void execute(Runnable command) {
        getDelegate().execute(command);
        capacityChecker.checkCapacity();
    }

    private static class ThreadPoolExecutorCapacityChecker extends CapacityChecker {
        private final ThreadPoolExecutor pool;
        private final LogDispatcher logDispatcher;

        public ThreadPoolExecutorCapacityChecker(ThreadPoolExecutor pool, LogDispatcher logDispatcher) {
            this.pool = pool;
            this.logDispatcher = logDispatcher;
        }

        @Override
        protected boolean atLowCapacity() {
            return getBusyThreadPercentage() > 75;
        }

        @Override
        protected void fireAlert() {
            int busyThreadPercentage = getBusyThreadPercentage();
            int queuePercentage = getQueuePercentage();
            Severity severity = queuePercentage > 10 ? Severity.FATAL : Severity.ERROR;
            Map<String, String> metrics = ImmutableMap.<String, String>builder()
                    .put("busyThreadPercentage", busyThreadPercentage + "")
                    .put("queuePercentage", queuePercentage + "")
                    .put("isShutdown", pool.isShutdown() + "")
                    .put("isTerminated", pool.isTerminated() + "")
                    .put("isTerminating", pool.isTerminating() + "")
                    .build();
            logDispatcher.report(this, severity, "Capacity alert on ThreadPoolExecutor: " + metrics.entrySet());
        }

        private int getBusyThreadPercentage() {
            return 100 * pool.getActiveCount() / pool.getMaximumPoolSize();
        }

        private int getQueuePercentage() {
            return 100 * pool.getQueue().size() / pool.getMaximumPoolSize();
        }
    }
}
