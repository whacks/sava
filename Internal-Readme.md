Integration Tests
=================

```mvn clean install -DrunIT```

Some integration tests may fail if not run by Project admins, since they rely on 3rd party credentials

Deployment
============

See [cava instructions](https://gitlab.com/whacks/cava/blob/master/Internal-Readme.md)

GPG
=========

See [cava instructions](https://gitlab.com/whacks/cava/blob/master/Internal-Readme.md)

Versioning
==========
See [cava versioning policy](https://gitlab.com/whacks/cava/blob/master/Internal-Readme.md)

Pending Tasks
==============
Cleanup of code-examples in README

More unit/integration tests. Most functionality has already been tested through the [Caucus](www.thecaucus.net) project, but it would be beneficial to have tests integrated into this repository for future development.

Repo cleanup.
