Sava: Simple Java
=================
Enable usage of popular services, libraries and modules, with minimal complexity.

Principles
==========
Provide easy path for the most common use cases.
 
Minimize the number of methods and options, in order to reduce cognitive load on developers.

Use best judgement to find the sweet spot between functionality and simplicity. Our opinions will never please everyone, and that is fine.

Examples
=========
Reading a file from S3, with retries:
```java
S3Interface s3Interface = S3Interface.get(credentialsProvider);
String contents = s3Interface.getFullContents(bucket, s3Path);
s3Interface.downloadFile(bucket, s3Path, outputFile);
```

Sending emails through SendGrid/Mandrill:
```java
Email email = Email.getMandrill(apiKey);
email.sendEmail(emailInfo);
email.blackList(email, subaccount);
```

Publishing events through SNSPublisher:
```java
Publisher.getSNS(awsCredentials).publish(topic, title, message);
```

S3 ApiKeys management:
```java
ApiKeysMux apiKeys = new ApiKeysMux(prodBucket, qaBucket, s3Interface);
apiKeys.useQA();    // Use for QA testing. Comment out for prod
String credentials = apiKeys.getCredentials(credentialsPath);
```

Login management through Stormpath
```java
StormPath loginClient = new StormPath(apiID, apiSecret, appName);
loginClient.createAccount(userName, email, password, name);
loginClient.verifyAccount(userNameOrEmail, password);
loginClient.resetPassword(email);
```

Web-Session Handling, using HMAC + AES:
```java
Session<YourData> newSession = new Session(data, validMinutes);
String secureToken = newSession.generateToken();
Session<YourData> parsedSession = Session.parseToken(secureToken);
YourData data = parsedSession.getSessionData();
```

Thread pools that facilitate quick and easy offloading of work, as well as capacity/error checking on threaded tasks
```java
ThreadHelper helper = new ThreadHelper(executor);
helper.execute(lambda);
Future<Boolean> future = helper.executeWithFuture(() -> getBool());
helper.close();
```

Logging with Severity-based actions:
```java
logDispatcher = LogDispatcher.build(dispatcher);
logDispatcher.info("log4j2");
logDispatcher.warn("log4j2 + email-alert");
logDispatcher.error("log4j2 + text-alert");
logDispatcher.fatal("log4j2 + ops-genie-alarm");
```

Maven Installation Guide
========================

Add the following to your pom.xml file, and you should be good to go. Check [here for latest version](https://gitlab.com/whacks/sava/blob/master/pom.xml).

```xml
<dependencies>
    <dependency>
        <groupId>com.rajivprab</groupId>
        <artifactId>sava</artifactId>
        <version>6.0.20</version>
    </dependency>
</dependencies>
```

About
=====
Sava was developed while building [Caucus](thecaucus.net). We realized that integrating with 3rd party services was much more complicated than we desired. Hence why we built our own internal wrappers to simplify the interface. We soon realized that these wrappers could be made into a generic library that any project can use. Thus was born the Sava library.
